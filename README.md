# Mostrini

Generatore di mostrini per Luca.

Griglia 5x5 simmetrica, le ultime due colonne sono il mirror delle prime due (variabilità di 15bit).

## Files

* `mostrini_random.py` - genera un mostrino a caso ad ogni interazione
* `mostrini_sequenziali.py` - genera tutti i mostrini sequenzialmente
* `mostrini_paralleli.py` - genera tutti i mostrini in parallelo usando più thread


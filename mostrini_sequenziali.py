#!/usr/bin/python3

import itertools
import matplotlib.pyplot as plt
import numpy as np
import os
import logging

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S')

def crea_img(indice):
    # creo array dalla stringa
    a = np.array(list(indice), dtype=int)
    # lo riarrangio 5x3
    a = np.reshape(a,(5,3))
    # aggiungo due colonne mirrorando le prime due
    a = np.hstack((a,a[:,1].reshape(5,1)))
    a = np.hstack((a,a[:,0].reshape(5,1)))
    logging.info("Genero " + indice + ".pdf - " + str(int(indice,2)) + "/32767" )
    img = plt.imshow(a, interpolation='nearest')
    img.set_cmap('hot')
    plt.axis('off')
    #plt.savefig(indice + ".png", bbox_inches='tight')
    plt.savefig(indice + ".pdf", format="pdf")

for i in ["".join(seq) for seq in itertools.product("01", repeat=15)]:
    crea_img(i)

logging.info("Finito.")
